
# consumet-graphql
[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=KuroXI_consumet-graphql)](https://sonarcloud.io/summary/new_code?id=KuroXI_consumet-graphql)

## Description

I integrated a REST API with GraphQL to improve data retrieval and manipulation for a web application. The motivation behind this project was to enhance the efficiency and flexibility of data access in our application.

We built this project because REST APIs have a fixed structure for data retrieval, and we needed a more dynamic way to fetch and modify data according to our application's evolving requirements.

The problem it solves is that it allows us to customize data requests and responses, reducing over-fetching or under-fetching of data. With GraphQL, clients can request only the specific data they need, reducing the load on the server and improving performance.

Through this project, I learned how to set up a GraphQL server and define schemas, resolvers, and queries. I also gained insights into the benefits of GraphQL in terms of data flexibility and efficiency. It was a valuable experience in working with modern API technologies and improving the overall performance and user experience of our web application.

## Installation

1. Clone the repository
```bash
git clone https://github.com/KuroXI/consumet-graphql.git
cd ./consumet-graphql
```
2. Install dependencies
```bash
npm install
```

## Usage
To start the development server and view the website locally, run the following command
```bash
npm run dev
```


### Vercel
Host your own instance of Consumet GraphQL on Vercel using the button below.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https://github.com/KuroXI/consumet-graphql)

## License

This project is licensed under the [GPL-3.0 license](https://github.com/KuroXI/consumet-graphql/blob/main/LICENSE).